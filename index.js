// http module -> allows node.js to transfer data
let http = require("http");


// port -> a virtual point where network connection start and end.
http.createServer(function(request, response) {

    //response -> 200 = ok
    /* response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Hello World!"); */

    response.writeHead(200, { "Content-Type": "text/html" });
    response.end("<h1>Hello World!</h1>");
}).listen(4000);

console.log("Server is running at localhost:4000");