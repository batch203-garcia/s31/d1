const http = require("http");

const port = 4000;


//req and res -> an object with property and methods
const server = http.createServer((req, res) => {
    //Creation of 2 end points -> /greeting and /homepage
    if (req.url == "/greeting") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Hello Again!");
    } else if (req.url == "/homepage") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("This is the home page.");
    } else {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Page is not available.");
    }
});

server.listen(port);

console.log(`Server is now accessible at localhost:${port}`);